<?php 

require("jsonparser/jsonparser.php");

$parser = new JsonParser();

$file_link= "jsonfile.json";

/* JsonParser provides a parse method which takes 
a file link to parse the json content */

try{
    
    $parsed_json_content = $parser->parse($file_link);
    print_r($parsed_json_content);

}catch(Exception $e){
    echo 'message: '.$e->getMessage();
}

?>