<?php 

require("jsonparserinterface.php");

class JsonParser implements JsonParserInterface{

    function __construct (){

      }
// override the parse method
    function parse($file_link){

        if (empty($file_link)){
           throw new InvalidArgumentException("file link can not be black");
        }

        if (file_exists($file_link)){
            try{
                // read file content
                $json_file_data = file_get_contents($file_link);
                // convert to json
                $json_data = json_decode($json_file_data,true);

                return $json_data;
        
                }catch(Exception $e){
                    // pass up the chain
                    throw $e;
                }
        }else{
            throw new Exception("file does not exist at {$file_link}");
        }
      }
  }

?>