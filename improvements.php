<?php

// Part 2: PHP Code logic and syntax understanding


// question 1 improved 

$users = [ 'John' ,  'Mike' ,  'George' ];

foreach ($users  as  $name) {
    echo $name;
  };

// question 2 improved 

if(isset($_POST[ ‘username’ ] )){
    include  $_POST[ ‘username’ ] .  '_user.inc.php' ;
}

// question 3 improved 

if(isset($_POST['username']) && isset($_POST[‘password’])  ) { 
 $username = mysql_real_escape_string($_POST['username']);
 $query = "SELECT * FROM users WHERE username = '{$username}' AND password = '{$_POST['password']}' LIMIT 1";
  $mysqli->query($query);
 }

 // question 4 improved 

 $users = [  
 'John'  => [],
 'Mike'  => [],
 'George'  => [] 
];

foreach ($users  as  $username => $userData) {
 printf( 'Hello %s, here are your account details: ' , $username). PHP_EOL;
 echo  $userData . PHP_EOL; 
}

// question 5 improved 

$username = preg_replace( '/^[^\w]+$/' ,  '' , $_GET[ 'username' ]);
 printf( 'Hello %s, how are you today?' , $username) . PHP_EOL;

// question 6 improved , for better readability 

use   HV \ UserManager \ Manager   as   UserManager;
use   HV \ UserManager \ User ;
use   HV \ UserManager \ Rank \ Rank ;
use   HV \ UserManager \ Rank \ List   as   RankList;

/**  @var  int $userId */

$User =  new  User($userId);
$Manager =  new  UserManager();
$Rank = new Rank(RankList::Moderator);
$Manager->setRank(Rank);
$Manager->commit();