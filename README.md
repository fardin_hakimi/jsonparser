jsonparser is a php class which exposes a parse method, this method takes a valid file link and returns 
parsed json data.


how to use: 

try{
$parser = new JsonParser();
$json_content = $parser.parse($your_file_link);
print_r($json_content);
}catch(Exception $e){
echo 'message: '.$e->getMessage();
}

